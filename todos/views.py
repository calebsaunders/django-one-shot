from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Count
from todos.forms import TodoListForm, TodoItemForm
from todos.models import TodoList, TodoItem
# Create your views here.

def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_update(request, id):
    todo_item = TodoItem.objects.get(id=id)
    print("Todo item is", todo_item)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
          
            todo_item = form.save(commit=False)
            print(form)
            print(request.POST.get('todo_list'))
            todo_list = form.cleaned_data['todo_list']
            todo_item.save()

           
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoItemForm(instance=todo_item)

    todo_lists = TodoList.objects.all()
    context = {
        'form': form,
        'item': todo_item,
        'todo_lists': todo_lists,  # Pass the associated TodoList instance
    }
    return render(request, 'todos/todo_item_update_form.html', context)
        
def todo_item_create(request):
    todo_lists = TodoList.objects.all()
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            print("form is valid")
            todo_item = form.save()
            todo_list = TodoList.objects.get(id=request.POST.get('list'))
          
            todo_item.list = TodoList.objects.get(id=todo_list.id)
            todo_item.save()
            return redirect('todo_list_detail',id  = todo_list.id)
    else:
        form = TodoItemForm()

    context = {
        'form': form,
        'todo_lists': todo_lists,
    }
    return render(request, 'todos/todo_item_form.html', context)

def todo_list_update(request,id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {form: form, "todo_list": todo_list}
  
    return render(request, "todos/edit.html", context=context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {'form': form}
    return render(request, "todos/create.html", context=context)

def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    items = TodoItem.objects.filter(list= todo_list)
    context = {
        "list": todo_list,
        "items": items,
    }
    return render(request, "todos/detail.html", context=context)