from django.db import models
from django.conf import settings

# Create your models here.




class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return f"{self.name}"

class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)  # Changed optional=True to null=True, blank=True
    is_completed = models.BooleanField(default=False)
    list = models.ForeignKey(
        "TodoList",  # Corrected the related model name
        related_name="items",  # Added a related_name for the reverse relation
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None,
    )

    def __str__(self):
        return f"{self.task}"