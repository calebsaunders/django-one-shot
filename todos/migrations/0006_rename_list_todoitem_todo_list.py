# Generated by Django 5.0.6 on 2024-06-11 19:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todos', '0005_rename_is_complete_todoitem_is_completed'),
    ]

    operations = [
        migrations.RenameField(
            model_name='todoitem',
            old_name='list',
            new_name='todo_list',
        ),
    ]
